<?php
include('header.php');
?>
<!-- Features -->
	<div id="features-wrapper">
		<div class="container">
			<div class="row">
				<div class="4u">
				
					<!-- Box -->
						<section class="box feature">
							<a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a>
							<div class="inner">
								<header>
									<h2>SAĞLIK</h2>
									<p>Maybe here as well I think</p>
								</header>
								<p>Phasellus quam turpis, feugiat sit amet in, hendrerit in lectus. Praesent sed semper amet bibendum tristique fringilla.</p>
							</div>
						</section>

				</div>
				<div class="4u">
				
					<!-- Box -->
						<section class="box feature">
							<a href="#" class="image featured"><img src="images/pic02.jpg" alt="" /></a>
							<div class="inner">
								<header>
									<h2>BİLİŞİM</h2>
									<p>This is also an interesting subtitle</p>
								</header>
								<p>Phasellus quam turpis, feugiat sit amet in, hendrerit in lectus. Praesent sed semper amet bibendum tristique fringilla.</p>
							</div>
						</section>

				</div>
				<div class="4u">
				
					<!-- Box -->
						<section class="box feature last">
							<a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
							<div class="inner">
								<header>
									<h2>ENERJİ</h2>
									<p>Here's another intriguing subtitle</p>
								</header>
								<p>Phasellus quam turpis, feugiat sit amet in, hendrerit in lectus. Praesent sed semper amet bibendum tristique fringilla.</p>
							</div>
						</section>

				</div>
			</div>
		</div>
	</div>

<!-- Main -->
	<div id="main-wrapper">
		<div class="container">
			<div class="row">
				<div class="4u">
				
					<!-- Sidebar -->
						<div id="sidebar">
							
						</div>
				
				</div>
				<div class="8u important(collapse)">

					<!-- Content -->
						<div id="content">
							<section class="last">
								<h2>Erasmus Nedir</h2>
								<p>This is Verti, a free and fully responsive HTML5 site template by <a href="http://html5up.net">HTML5 UP</a>.
								Verti is released under the Creative Commons Attribution license</a>, so feel free to use it for any personal or commercial project you might have going on (just don't forget to credit us for the design!)</p>
								<p>Phasellus quam turpis, feugiat sit amet ornare in, hendrerit in lectus. Praesent semper bibendum ipsum, et tristique augue fringilla eu. Vivamus id risus vel dolor auctor euismod quis eget mi. Etiam eu ante risus. Aliquam erat volutpat. Aliquam luctus mattis lectus sit amet phasellus quam turpis.</p>
							</section>
						</div>

				</div>
			</div>
		</div>
	</div>


	</div>
