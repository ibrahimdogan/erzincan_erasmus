<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<form class="form-horizontal" action="" method="POST">
<fieldset>

<!-- Form Name -->
<legend>Bütçe Ekle</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Proje Seç</label>
  <div class="col-md-4">
    <select id="selectbasic" name="proje_adi" class="form-control">
    <?php
            include('baglanti.php');
            $query = $db->query("SELECT * FROM projeler", PDO::FETCH_ASSOC);
            if ( $query->rowCount() ){
                 foreach( $query as $row ){?>
                      <option value=<?php echo $row['adi'];?>><?php echo $row['adi'];?></option>
               <?php  }
            }
            ?>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="miktar">Miktar</label>  
  <div class="col-md-4">
  <input id="miktar" name="miktar"  type="text" placeholder="Para miktarı giriniz..." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="birim">birim sec</label>
  <div class="col-md-4">
    <select id="birim" name="birim" onchange="cevir();"  class="form-control" required="">
    <option value="0">birim sec</option> 
    <option value="1">TL</option>
      <option value="2">dolar</option>
      <option value="3">Euro</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tl_cevir">TL Dönüşüm</label>  
  <div class="col-md-4">
  <input id="tl_cevir" name="tl_cevir" type="text" placeholder="TL karşılığı" class="form-control input-md">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="aciklama">Açıklama</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="aciklama" name="aciklama"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button id="" name="" class="btn btn-primary">Bütçe Ekle</button>
  </div>
</div>

</fieldset>
</form>
<script>
    function cevir(){
      
        var miktar=document.getElementById('miktar').value;
        var tl_cevir=document.getElementById('tl_cevir');
        var birim=document.getElementById('birim').value;
           var a=parseInt(miktar);
           if(birim==2){
               tl_cevir.value=a/5;
           }
           else if(birim==3){
            tl_cevir.value=a/5;
           }
           else if(birim==1){
            tl_cevir.value=a;
           }
           
    }
    </script>


<?php
if(isset($_POST['aciklama'])){
    include('baglanti.php');
    $proje_adi=$_POST['proje_adi'];
    $miktar=$_POST['tl_cevir'];
    $aciklama=$_POST['aciklama'];

    $query = $db->prepare("INSERT INTO hesap SET
    proje_adi = ?,
    miktar = ?,
    aciklama = ?");
    $insert = $query->execute(array(
        $proje_adi,$miktar, $aciklama
    ));
    if ( $insert ){
        $last_id = $db->lastInsertId();
        header('location:index.php');
        
    }
}
?>