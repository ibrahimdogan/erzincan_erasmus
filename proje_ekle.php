<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<form class="form-horizontal" action="proje_db_kaydet.php" method="post">
<fieldset>

<!-- Form Name -->
<legend>PROJE KAYIT EKRANI</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="adi">Proje Adı</label>  
  <div class="col-md-4">
  <input id="adi" name="adi" type="text" placeholder="proje adı girin.." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="proje_kodu">Proje kodu</label>  
  <div class="col-md-4">
  <input id="proje_kodu" name="proje_kodu" type="text" placeholder="Proje kodu giriniz" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="baglangic_trh">Başlangıç tarihi</label>  
  <div class="col-md-4">
  <input id="baglangic_trh" name="baslangic_trh" type="date" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="bitis_trh">Bitiş Tarihi</label>  
  <div class="col-md-4">
  <input id="bitis_trh" name="bitis_trh" type="Date" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="koordinator">Koordinatör</label>  
  <div class="col-md-4">
  <input id="koordinator" name="koordinator" type="text" placeholder="Koordinator girin..." class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="odeme_yetkili">Ödeme yetkilisi</label>  
  <div class="col-md-4">
  <input id="odeme_yetkili" name="odeme_yetkili" type="text" placeholder="Yekili girin..." class="form-control input-md">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="textarea">ACIKLAMA</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="textarea" name="aciklama"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button id="" name="" class="btn btn-success">Kaydet</button>
  </div>
</div>

</fieldset>
</form>
